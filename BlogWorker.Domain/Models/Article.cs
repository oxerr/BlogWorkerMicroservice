﻿using System;

namespace BlogWorker.Domain.Models
{
    public class Article
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Url { get; set; }
        public bool IsPublished { get; set; }
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }

        public DateTime? UpdatedAt { get; set; }
        public DateTime? PublishedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public int SiteId { get; set; }
        public List<ArticleImage> ArticleImages { get; set; }
        public Article(string title,
            string content,
            string url,
            string isPublished,
            string metaTitle,
            string metaDescription,
            DateTime updatedAt,
            DateTime publishedDate,
            DateTime createdDate,
            int siteId)
        {
            this.Title = title;
            this.Content = content;
            this.Url = url;
            this.IsPublished = IsPublished;
            this.MetaTitle = metaTitle;
            this.MetaDescription = metaDescription;
            this.UpdatedAt = updatedAt; 
            this.PublishedDate = publishedDate;
            this.CreatedDate = createdDate;
            this.SiteId = siteId;
        }

        public Article() { }
    }
}
