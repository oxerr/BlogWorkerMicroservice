﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogWorker.Domain.Models
{
    public class ArticleCategories
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public int ArticleId { get; set; }

        public ArticleCategories(int categoryId, int articleId)
        {
            this.CategoryId = categoryId;
            this.ArticleId = articleId;
        }
    }
}
