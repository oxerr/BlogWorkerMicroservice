﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogWorker.Domain.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsAvailable { get; set; }
        public int SiteId { get; set; }

        public Category(string name, string description, bool isAvailable, int siteId)
        {
            this.Name = name;
            this.Description = description;
            this.IsAvailable = isAvailable;
            this.SiteId = siteId;
        }

        public Category()
        {            
        }
    }
}
