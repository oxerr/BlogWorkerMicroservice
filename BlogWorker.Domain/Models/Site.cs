﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogWorker.Domain.Models
{
    public class Site
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Domain { get; set; }
        public string About { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string FacebookUrl { get; set; }
        public string TwitterUrl { get; set; }
        public string InstagramUrl { get; set; }
    }
}
