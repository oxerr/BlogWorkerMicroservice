﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogWorker.Domain.Repositories
{
    public interface ISiteRepository<T> where T : class
    {
        Task<T> GetSiteById(int id);
        Task<T> GetByName(string name);
    }
}
