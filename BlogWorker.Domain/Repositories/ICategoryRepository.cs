﻿using BlogWorker.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogWorker.Domain.Repositories
{
    public interface ICategoryRepository<T> : IRepository<T> where T : class
    {
        public Task<IEnumerable<Category>> GetAllBySiteName(string applicationName);
    }
}
