﻿using BlogWorker.Domain.Models;

namespace BlogWorker.Domain.Repositories
{
    public interface IArticleRepository<T> : IRepository<T> where T : class
    {
        public Task<IEnumerable<Article>> GetAllBySiteName(string applicationName);
    }
}
