﻿using BlogWorker.Domain.Models;
using BlogWorker.Domain.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BlogWorkerAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArticleCategoryController : ControllerBase
    {
        private IArticleCategoriesRepository<ArticleCategories> _articleCategoryRepository { get; }

        public ArticleCategoryController(IArticleCategoriesRepository<ArticleCategories> _articleCategoryRepository)
        {
            this._articleCategoryRepository = _articleCategoryRepository;
        }

        [HttpGet("GetArticlesByCategory/{id}")]
        public async Task<IEnumerable<Article>> GetArticlesByCategoryId(int id)
        {
            var articlesInCategory = await this._articleCategoryRepository.GetAllArticleByCategoryId(id);
            return articlesInCategory;
        }

        [HttpPost("DeleteArticleCategoryRelation")]
        public async Task DeleteArticleCategoryRelation(int id)
        {
            var relation = await this._articleCategoryRepository.GetById(id);
            await this._articleCategoryRepository.Delete(relation);
        }

        [HttpPost("AddArticleCategoryRelation")]
        public async Task AddArticleCategoryRelation(int categoryId, int articleId)
        {
            ArticleCategories articleCategories = new ArticleCategories(categoryId, articleId);
            await this._articleCategoryRepository.Add(articleCategories);
        }


    }
}
