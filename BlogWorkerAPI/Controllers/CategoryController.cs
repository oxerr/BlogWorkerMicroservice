﻿using BlogWorker.Domain.Models;
using BlogWorker.Domain.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BlogWorkerAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private ICategoryRepository<Category> _categoryRepository { get; }
        private ISiteRepository<Site> _siteRepository { get; }

        public CategoryController(ICategoryRepository<Category> _categoryRepository, ISiteRepository<Site> siteRepository)
        {
            this._categoryRepository = _categoryRepository;
            _siteRepository = siteRepository;
        }

        [HttpGet("GetBy/{id}")]
        public async Task<Category> GetCategoryById(int id)
        {
            var category = await this._categoryRepository.GetById(id);
            return category;
        }

        [HttpGet("GetAllCategories")]
        public async Task<IEnumerable<Category>> GetAllCategories()
        {
            var categories = await this._categoryRepository.GetAll();
            return categories;
        }

        [HttpGet("GetAllCategoriesBySite")]
        public async Task<IEnumerable<Category>> GetAllCategoriesBySite(string applicationName)
        {
            var categories = await this._categoryRepository.GetAllBySiteName(applicationName);
            return categories;
        }

        [HttpPost("AddCategory")]
        public async Task<IActionResult> AddCategory(Category category, string applicationName)
        {
            try
            {
                var site = await this._siteRepository.GetByName(applicationName);
                if (site != null)
                {
                    category.SiteId = site.Id;
                    await this._categoryRepository.Add(category);
                    return Ok("Category added successfully.");
                }
                else
                {
                    return NotFound("Site not found.");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred: {ex.Message}");
            }
        }

        [HttpPost("UpdateCategory")]
        public async Task UpdateCategory(Category category)
        {
            await this._categoryRepository.Update(category);
        }


    }
}
