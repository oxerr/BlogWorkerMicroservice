﻿using BlogWorker.Domain.Models;
using BlogWorker.Domain.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BlogWorkerAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArticleController : ControllerBase
    {
        private IArticleRepository<Article> _articleRepository { get; }
        private ISiteRepository<Site> _siteRepository { get; }

        public ArticleController(IArticleRepository<Article> _articleRepository, ISiteRepository<Site> siteRepository)
        {
            this._articleRepository = _articleRepository;
            _siteRepository = siteRepository;
        }

        [HttpGet("GetBy/{id}")]
        public async Task<Article> GetArticleById(int id)
        {
            var article = await this._articleRepository.GetById(id);
            return article;
        }

        [HttpGet("GetAll")]
        public async Task<IEnumerable<Article>> GetAllArticles()
        {
            var articles = await this._articleRepository.GetAll();
            return articles;
        }

        [HttpGet("GetAllArticlesBySite")]
        public async Task<IEnumerable<Article>> GetAllArticlesBySite(string applicationName)
        {
            var articles = await this._articleRepository.GetAllBySiteName(applicationName);
            return articles;
        }

        [HttpPost("AddArticle")]
        public async Task<IActionResult> AddArticle(Article article, string applicationName)
        {
            try
            {
                var site = await this._siteRepository.GetByName(applicationName);
                if (site != null)
                {
                    article.SiteId = site.Id;
                    await this._articleRepository.Add(article);
                    return Ok();
                }
                else return NotFound();
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred: {ex.Message}");
            }
        }

        [HttpPost("DeleteArticle")]
        public async Task<IActionResult> DeleteArticle(int id)
        {
            try
            {
                var article = await this._articleRepository.GetById(id);

                if (article == null)
                {
                    return NotFound();
                }

                await this._articleRepository.Delete(article);

                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "An error during delete article request");
            }
        }

        [HttpPost("UpdateArticle")]
        public async Task UpdateArticle(Article article)
        {
            await this._articleRepository.Update(article);
        }

    }
}
