﻿using BlogWorker.Domain.Models;
using BlogWorker.Domain.Repositories;
using BlogWorker.Infrastucture.Repositories;
using Microsoft.Data.SqlClient;
using System.Data;
using System;
using Npgsql;

namespace BlogWorkerAPI.Components
{
    public static class RepositoryHelper
    {
        public static void RegisterRepositories(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("DB");
            connectionString = ConnectionStringBuilder(connectionString);
            services.AddTransient<IDbConnection>((sp) => new NpgsqlConnection(connectionString));
            services.AddTransient(typeof(IArticleRepository<Article>), typeof(ArticleRepository));
            services.AddTransient(typeof(ICategoryRepository<Category>), typeof(CategoryRepository));
            services.AddTransient(typeof(IArticleCategoriesRepository<ArticleCategories>), typeof(ArticleCategoryRepository));
            services.AddTransient(typeof(ISiteRepository<Site>), typeof(SiteRepository));
        }

        private static string ConnectionStringBuilder(string connectionString)
        {
            var dbHost = Environment.GetEnvironmentVariable("PGSQL_DB_HOST");
            var dbPort = Environment.GetEnvironmentVariable("PGSQL_DB_PORT");
            connectionString = connectionString.Replace("${PGSQL_DB_HOST}", dbHost);
            connectionString = connectionString.Replace("${PGSQL_DB_PORT}", dbPort);
            return connectionString;
        }
    }
}
