﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogWorker.Infrastucture.RepositoryQueries
{
    public class SiteQueries
    {
        private const string CreateSiteTableQuery = @"CREATE TABLE Site
                                                        (
                                                            Id INT PRIMARY KEY IDENTITY(1,1),
                                                            Name NVARCHAR(MAX),
                                                            Description NVARCHAR(MAX),
                                                            CreatedAt DATETIME,
                                                            Domain NVARCHAR(MAX),
                                                            About NVARCHAR(MAX),
                                                            EmailAddress NVARCHAR(MAX),
                                                            PhoneNumber NVARCHAR(MAX),
                                                            MetaTitle NVARCHAR(MAX),
                                                            MetaDescription NVARCHAR(MAX),
                                                            FacebookUrl NVARCHAR(MAX),
                                                            TwitterUrl NVARCHAR(MAX),
                                                            InstagramUrl NVARCHAR(MAX)
                                                        );";

        private const string CreateSiteTablePostgresQuery = @"CREATE TABLE Site
                                                            (
                                                                Id SERIAL PRIMARY KEY,
                                                                Name VARCHAR,
                                                                Description VARCHAR,
                                                                CreatedAt TIMESTAMP,
                                                                Domain VARCHAR,
                                                                About VARCHAR,
                                                                EmailAddress VARCHAR,
                                                                PhoneNumber VARCHAR,
                                                                MetaTitle VARCHAR,
                                                                MetaDescription VARCHAR,
                                                                FacebookUrl VARCHAR,
                                                                TwitterUrl VARCHAR,
                                                                InstagramUrl VARCHAR
                                                            );";

        private const string InsertRandomValuesQuery = @"INSERT INTO Site (Name, Description, CreatedAt, Domain, About, EmailAddress, PhoneNumber, MetaTitle, MetaDescription, FacebookUrl, TwitterUrl, InstagramUrl)
                                                            VALUES
                                                                ('Blogpost', 'Description 1', GETDATE(), 'blogpost.com', 'About site 1', 'contact@site1.com', '123-456-7890', 'Meta Title 1', 'Meta Description 1', 'facebook.com/site1', 'twitter.com/site1', 'instagram.com/site1'),
                                                                ('Autopost', 'Description 2', GETDATE(), 'autopost.com', 'About site 2', 'contact@site2.com', '987-654-3210', 'Meta Title 2', 'Meta Description 2', 'facebook.com/site2', 'twitter.com/site2', 'instagram.com/site2');
                                                            ";

        public const string GetSiteByIdQuery = @"SELECT * FROM Site WHERE Id = @id";
        public const string GetSiteByNameQuery = @"SELECT * FROM Site WHERE Name = @name";
    }
}
