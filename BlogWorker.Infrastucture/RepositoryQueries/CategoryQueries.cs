﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogWorker.Infrastucture.RepositoryQueries
{
    public class CategoryQueries
    {
        private const string CreateCategoryTableQuery = @"CREATE TABLE Category
                                                        (
                                                            Id INT PRIMARY KEY IDENTITY(1, 1),
                                                            Name NVARCHAR(MAX) NOT NULL,
                                                            Description NVARCHAR(MAX),
                                                            IsAvailable bit,
                                                            FOREIGN KEY (SiteId) REFERENCES Site(Id)
                                                        );";

        private const string CreateCategoryTablePostgresQuery = @"CREATE TABLE Category
                                                                (
                                                                    Id SERIAL PRIMARY KEY,
                                                                    Name TEXT NOT NULL,
                                                                    Description TEXT,
                                                                    IsAvailable BOOLEAN,
                                                                    SiteId INT,
                                                                    FOREIGN KEY (SiteId) REFERENCES Site(Id)
                                                                );
                                                                ";

        private const string InsertRandomIntoCategoryTableQuery = @"INSERT INTO Category (Name, Description, IsAvailable, SiteId)
                                                                    VALUES
                                                                        ('Category 1', 'Description for Category 1', 1, 1),
                                                                        ('Category 2', 'Description for Category 2', 0, 1),
                                                                        ('Category 3', 'Description for Category 3', 1, 2),
                                                                        ('Category 4', 'Description for Category 4', 0, 1),
                                                                        ('Category 5', 'Description for Category 5', 1, 2);";

        private const string InsertRandomIntoCategoryTablePostgresQuery = @"INSERT INTO Category (Name, Description, IsAvailable, SiteId)
                                                                            VALUES
                                                                                ('Category 1', 'Description for Category 1', TRUE, 1),
                                                                                ('Category 2', 'Description for Category 2', FALSE, 1),
                                                                                ('Category 3', 'Description for Category 3', TRUE, 2),
                                                                                ('Category 4', 'Description for Category 4', FALSE, 1),
                                                                                ('Category 5', 'Description for Category 5', TRUE, 2);
                                                                            ";

        public const string GetCategoryByIdQuery = @"SELECT * FROM Category WHERE Id = @id";
        public const string GetAllCategoriesQuery = @"SELECT * FROM Category";
        public const string GetAllCategoriesBySiteNameQuery = @"SELECT c.* FROM Category c
                                                                JOIN Site s on s.Id = c.SiteId
                                                                WHERE s.Name = @applicationName";
        public const string AddCategoryQuery = @"INSERT INTO Category(Name, Description, IsAvailable, SiteId) VALUES(@name, @description, @isAvailable, @siteId)";
        public const string UpdateCategoryQuery = @"UPDATE Category SET Name = @name, Description = @description, IsAvailable = @isAvailable WHERE Id = @id";
        public const string DeleteCategoryQuery = @"DELETE FROM Category WHERE Id = @id";
    }
}
