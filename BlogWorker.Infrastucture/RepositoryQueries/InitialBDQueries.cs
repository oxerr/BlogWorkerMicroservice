﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogWorker.Infrastucture.RepositoryQueries
{
    internal class InitialBDQueries
    {
        private const string CreateUserQuery = @"
                                                USE master;
                                                GO

                                                -- Создание логина с более сложным паролем
                                                CREATE LOGIN blog_worker WITH PASSWORD = 'ComplexPassword123!'; -- здесь используйте пароль, соответствующий требованиям

                                                USE CommonDB;
                                                GO

                                                -- Создание пользователя и назначение ролей
                                                CREATE USER blog_worker FOR LOGIN blog_worker;
                                                GO

                                                -- Назначение ролей пользователю
                                                ALTER ROLE db_datareader ADD MEMBER blog_worker;
                                                ALTER ROLE db_datawriter ADD MEMBER blog_worker;
                                                ";

        private const string AddRoleAndRulesForUser = @"
                                                USE master;
                                                SELECT name FROM sys.sql_logins WHERE name = 'blog_worker';

                                                USE CommonDB;
                                                SELECT name FROM sys.database_principals WHERE name = 'blog_worker';

                                                USE CommonDB;
                                                -- Проверка разрешений SELECT (db_datareader)
                                                EXEC sp_helpuser 'blog_worker';
                                                -- Проверка разрешений INSERT, UPDATE, DELETE (db_datawriter)
                                                EXEC sp_helprolemember 'db_datawriter';

                                                USE CommonDB;
                                                -- Назначение разрешения SELECT (db_datareader) пользователю
                                                GRANT SELECT ON SCHEMA::dbo TO blog_worker;
                                                -- Назначение разрешения INSERT, UPDATE, DELETE (db_datawriter) пользователю
                                                GRANT INSERT, UPDATE, DELETE ON SCHEMA::dbo TO blog_worker;

                                                USE CommonDB;
                                                SELECT IS_MEMBER('db_datawriter') AS IsMember;
                                                    ";
    }
}
