﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogWorker.Infrastucture.RepositoryQueries
{
    public class ArticleQueries
    {
        private const string CreateArticleTable = @"CREATE TABLE Article
                                                    (
                                                        Id INT PRIMARY KEY IDENTITY(1,1),
                                                        SiteId INT,
                                                        Title NVARCHAR(MAX),
                                                        Content NVARCHAR(MAX),
                                                        Url NVARCHAR(MAX),
                                                        IsPublished BIT,
                                                        MetaTitle NVARCHAR(MAX),
                                                        MetaDescription NVARCHAR(MAX),
                                                        UpdatedAt DATETIME,
                                                        PublishedDate DATETIME,
                                                        CreatedDate DATETIME,
                                                        FOREIGN KEY (SiteId) REFERENCES Site(Id)
                                                    );";

        private const string CreateArticleTablePostgresQuery = @"CREATE TABLE Article
                                                                (
                                                                    Id SERIAL PRIMARY KEY,
                                                                    SiteId INT,
                                                                    Title TEXT,
                                                                    Content TEXT,
                                                                    Url TEXT,
                                                                    IsPublished BOOLEAN,
                                                                    MetaTitle TEXT,
                                                                    MetaDescription TEXT,
                                                                    UpdatedAt TIMESTAMP,
                                                                    PublishedDate TIMESTAMP,
                                                                    CreatedDate TIMESTAMP,
                                                                    FOREIGN KEY (SiteId) REFERENCES Site(Id)
                                                                );
                                                                ";

        private const string InsertRandomArticlesQuery = @"INSERT INTO Article (SiteId, Title, Content, Url, IsPublished, MetaTitle, MetaDescription, UpdatedAt, PublishedDate, CreatedDate)
                                                                VALUES
                                                                (1, 'Article 1', 'This is the content of Article 1.', 'https://www.example.com/article-1', 1, 'Article 1 Title', 'Article 1 Description', GETDATE(), GETDATE(), GETDATE())
                                                                (1, 'Article 2', 'This is the content of Article 2.', 'https://www.example.com/article-2', 1, 'Article 2 Title', 'Article 2 Description', GETDATE(), GETDATE(), GETDATE())
                                                                (2, 1'Article 3', 'This is the content of Article 3.', 'https://www.example.com/article-3', 1, 'Article 3 Title', 'Article 3 Description', GETDATE(), GETDATE(), GETDATE())
                                                                (1, 'Article 4', 'This is the content of Article 4.', 'https://www.example.com/article-4', 1, 'Article 4 Title', 'Article 4 Description', GETDATE(), GETDATE(), GETDATE())
                                                                (2, 'Article 5', 'This is the content of Article 5.', 'https://www.example.com/article-5', 1, 'Article 5 Title', 'Article 5 Description', GETDATE(), GETDATE(), GETDATE());
                                                                ";


        public const string GetArticleById = @" SELECT a.*, ai.Id, ai.Name, ai.Path, ai.ArticleId
                                                FROM Article a
                                                LEFT JOIN ArticleImage ai ON a.Id = ai.ArticleId
                                                WHERE a.Id = @id";

        public const string GetAllArticles = @" SELECT a.*, ai.Id, ai.Name, ai.Path, ai.ArticleId
                                                FROM Article a
                                                LEFT JOIN ArticleImage ai ON a.Id = ai.ArticleId";

        public const string GetAllArticlesBySite = @"   SELECT a.*, ai.Id, ai.Name, ai.Path, ai.ArticleId
                                                        FROM Article a
                                                        JOIN Site s ON s.Id = a.SiteId
                                                        LEFT JOIN ArticleImage ai ON a.Id = ai.ArticleId
                                                        WHERE s.Name = @applicationName";

        public const string GetAvailableArticlesBySite = @"SELECT a.* FROM Article a
                                                        JOIN Site s on s.Id = a.SiteId
                                                        WHERE s.Name = @applicationName
                                                        AND IsPublished = true";

        public const string InsertNewArticleQuery = @"INSERT INTO Article (SiteId, Title, Content, Url, IsPublished, MetaTitle, MetaDescription, UpdatedAt, PublishedDate, CreatedDate)
                                                                VALUES
                                                                (@siteId, @title, @content, @url, @isPublished, @metaTitle, @metaDescription, @updatedAt, @publishedDate, @createdDate)";

        public const string UpdateArticleByIdQuery = @"UPDATE Article
                                                        SET  Title = @title,
                                                        Content = @content,
                                                        Url = @url,
                                                        IsPublished = @isPublished,
                                                        MetaTitle = @metaTitle,
                                                        MetaDescription = @metaDescription,
                                                        UpdatedAt = @updatedAt,
                                                        PublishedDate = @publishedDate,
                                                        CreatedDate = @createdDate
                                                        WHERE Id = @id";

        public const string DeleteArticleByIdQuery = @"DELETE FROM Article WHERE Id = @id";

    }
}
