﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogWorker.Infrastucture.RepositoryQueries
{
    public class ArticleCategoryQueries
    {
        private const string CreateArticleCategoryTableQuery = @"CREATE TABLE ArticleCategories
                                                                (
                                                                    Id INT PRIMARY KEY IDENTITY(1, 1),
                                                                    CategoryId INT NOT NULL,
                                                                    ArticleId INT NOT NULL,
                                                                    FOREIGN KEY (CategoryId) REFERENCES Category(Id),
                                                                    FOREIGN KEY (ArticleId) REFERENCES Article(Id)
                                                                );";

        private const string CreateArticleCategoryPostgresQuery = @"CREATE TABLE ArticleCategories
                                                                    (
                                                                        Id SERIAL PRIMARY KEY,
                                                                        CategoryId INT NOT NULL,
                                                                        ArticleId INT NOT NULL,
                                                                        FOREIGN KEY (CategoryId) REFERENCES Category(Id),
                                                                        FOREIGN KEY (ArticleId) REFERENCES Article(Id)
                                                                    );
                                                                    ";

        private const string InsertRandomArticleCategoryQuery = @"INSERT INTO ArticleCategories (CategoryId, ArticleId)
                                                                VALUES
                                                                    (1, 1), 
                                                                    (2, 2), 
                                                                    (3, 3), 
                                                                    (4, 4), 
                                                                    (5, 5);
                                                                ";



        public const string GetAllArticlesByCategoryId = @"SELECT a.*
                                                            FROM Article a
                                                            JOIN ArticleCategories ac ON ac.ArticleId = a.Id
                                                            WHERE ac.CategoryId = @categoryId;";

        public const string GetArticleCategoryRelationById = @"SELECT * FROM ArticleCategories where Id = @id";

        public const string IsertIntoArticleCategoryQuery = @"INSERT INTO ArticleCategories (CategoryId, ArticleId) VALUES(@categoryId, @articleId)";
        
        public const string DeleteArticleCategoryEntityQuery = @"DELETE FROM ArticleCategories 
                                                                    WHERE CategoryId = @categoryId AND ArticleId = @articleId";

        public const string UpdateArticleCategoryQuery = @"UPDATE ArticleCategories SET CategoryId = @categoryId WHERE Id = @id";
    }
}
