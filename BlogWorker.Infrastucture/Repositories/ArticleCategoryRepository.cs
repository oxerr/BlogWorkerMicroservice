﻿using BlogWorker.Domain.Models;
using BlogWorker.Domain.Repositories;
using Dapper;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlogWorker.Infrastucture.RepositoryQueries;
using System.Collections;

namespace BlogWorker.Infrastucture.Repositories
{
    public class ArticleCategoryRepository : IArticleCategoriesRepository <ArticleCategories>
    {
        private IDbConnection Connection { get; }

        public ArticleCategoryRepository(IDbConnection dbConnection)
        {
            this.Connection = dbConnection;
        }

        public async Task Add(ArticleCategories entity)
        {
            var query = ArticleCategoryQueries.IsertIntoArticleCategoryQuery;
            await this.Connection.ExecuteAsync(query, new { entity.CategoryId, entity.ArticleId });
        }

        public async Task Update(ArticleCategories entity)
        {
            var query = ArticleCategoryQueries.UpdateArticleCategoryQuery;
            await this.Connection.ExecuteAsync(query, new { entity.CategoryId, entity.Id });
        }

        public async Task Delete(ArticleCategories entity)
        {
            var query = ArticleCategoryQueries.DeleteArticleCategoryEntityQuery;
            await this.Connection.ExecuteAsync(query, new { entity.CategoryId, entity.ArticleId });
        }

        public async Task <IEnumerable<Article>> GetAllArticleByCategoryId(int categoryId)
        {
            var query = ArticleCategoryQueries.GetAllArticlesByCategoryId;
            var articleCategoryRelation = await this.Connection.QueryAsync<Article>(query, new { categoryId });
            return articleCategoryRelation;
        }

        public Task<IEnumerable<ArticleCategories>> GetAll()
        {
            throw new NotImplementedException();
        }

        async Task<ArticleCategories> IRepository<ArticleCategories>.GetById(int id)
        {
            var query = ArticleCategoryQueries.GetArticleCategoryRelationById;
            var relation = await this.Connection.QueryFirstOrDefaultAsync<ArticleCategories>(query, new { id });
            return relation;
        }
    }
}
