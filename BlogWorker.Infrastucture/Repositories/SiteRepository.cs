﻿using BlogWorker.Domain.Models;
using BlogWorker.Domain.Repositories;
using BlogWorker.Infrastucture.RepositoryQueries;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogWorker.Infrastucture.Repositories
{
    public class SiteRepository : ISiteRepository<Site>
    {
        private IDbConnection Connection { get; }

        public SiteRepository(IDbConnection dbConnection)
        {
            this.Connection = dbConnection;
        }

        public async Task<Site> GetSiteById(int id)
        {
            var query = SiteQueries.GetSiteByIdQuery;

            var site = await this.Connection.QueryFirstOrDefaultAsync<Site>(query, new {id});
            return site;
        }

        public async Task<Site> GetByName(string name)
        {
            var query = SiteQueries.GetSiteByNameQuery;

            var site = await this.Connection.QueryFirstOrDefaultAsync<Site>(query, new { name });
            return site;
        }
    }
}
