﻿using BlogWorker.Domain.Models;
using BlogWorker.Domain.Repositories;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogWorker.Infrastucture.Repositories
{
    public class ArticleRepository : IArticleRepository<Article>
    {
        private IDbConnection Connection { get; }

        public ArticleRepository(IDbConnection dbConnection)
        {
            this.Connection = dbConnection;
        }


        public async Task Add(Article entity)
        {
            var query = RepositoryQueries.ArticleQueries.InsertNewArticleQuery;

            await this.Connection.ExecuteAsync(query, new
            {
                entity.Title,
                entity.Content,
                entity.Url,
                entity.IsPublished,
                entity.MetaTitle,
                entity.MetaDescription,
                entity.UpdatedAt,
                entity.PublishedDate,
                entity.CreatedDate,
                entity.SiteId
            });
        }

        public async Task Delete(Article entity)
        {
            var query = RepositoryQueries.ArticleQueries.DeleteArticleByIdQuery;

            await this.Connection.ExecuteAsync(query, new { entity.Id });
        }

        public async Task<IEnumerable<Article>> GetAll()
        {
            var query = RepositoryQueries.ArticleQueries.GetAllArticles;

            var articles = await this.Connection.QueryAsync<Article, ArticleImage, Article>(
                query,
                (article, articleImage) =>
                {
                    // Преобразование статьи и изображения
                    article.ArticleImages = new List<ArticleImage> { articleImage };
                    return article;
                },
                splitOn: "Id"
            );
            // Группировка статей и объединение изображений
            var groupedArticles = articles.GroupBy(a => a.Id)
                .Select(group =>
                {
                    var article = group.First();
                    article.ArticleImages = group.Select(a => a.ArticleImages.FirstOrDefault()).ToList();
                    return article;
                });

            return groupedArticles;
        }

        public async Task<IEnumerable<Article>> GetAllBySiteName(string applicationName)
        {
            var query = RepositoryQueries.ArticleQueries.GetAllArticlesBySite;

            var articlesDictionary = new Dictionary<int, Article>();
            var articles = await this.Connection.QueryAsync<Article, ArticleImage, Article>(
                query,
                (article, articleImage) =>
                {
                    if (!articlesDictionary.TryGetValue(article.Id, out var existingArticle))
                    {
                        existingArticle = article;
                        existingArticle.ArticleImages = new List<ArticleImage>();
                        articlesDictionary.Add(existingArticle.Id, existingArticle);
                    }
                    existingArticle.ArticleImages.Add(articleImage);
                    return existingArticle;
                },
                new { applicationName }, // Передача параметра applicationName
                splitOn: "Id"
            );

            return articles.Distinct();
        }


        public async Task<Article> GetById(int id)
        {
            var query = RepositoryQueries.ArticleQueries.GetArticleById;

            Article result = null;

            await this.Connection.QueryAsync<Article, ArticleImage, Article>(
                query,
                (article, articleImage) =>
                {
                    if (result == null)
                    {
                        result = article;
                        result.ArticleImages = new List<ArticleImage>();
                    }
                    if (articleImage != null)
                    {
                        result.ArticleImages.Add(articleImage);
                    }
                    return result;
                },
                new { id }, // Передача параметра id
                splitOn: "Id"
            );

            return result;
        }


        public async Task Update(Article entity)
        {
            var query = RepositoryQueries.ArticleQueries.UpdateArticleByIdQuery;

            await this.Connection.ExecuteAsync(query, new
            {
                entity.Title,
                entity.Content,
                entity.Url,
                entity.IsPublished,
                entity.MetaTitle,
                entity.MetaDescription,
                entity.UpdatedAt,
                entity.PublishedDate,
                entity.CreatedDate,
                entity.Id
            });
        }
    }
}
