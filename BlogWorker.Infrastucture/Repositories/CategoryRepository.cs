﻿using BlogWorker.Domain.Models;
using BlogWorker.Domain.Repositories;
using BlogWorker.Infrastucture.RepositoryQueries;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogWorker.Infrastucture.Repositories
{
    public class CategoryRepository : ICategoryRepository<Category>
    {
        private IDbConnection Connection { get; }

        public CategoryRepository(IDbConnection dbConnection)
        {
            this.Connection = dbConnection;
        }


        public async Task Add(Category category)
        {
            var query = CategoryQueries.AddCategoryQuery;
            await this.Connection.ExecuteAsync(query, new {category.Name, category.Description, category.IsAvailable, category.SiteId});
        }

        public async Task Delete(Category entity)
        {
            var query = CategoryQueries.DeleteCategoryQuery;            
            await this.Connection.ExecuteAsync(query, new {entity.Id});
        }

        public Task<IEnumerable<Category>> GetAll()
        {
            var query = CategoryQueries.GetAllCategoriesQuery;
            var categories = this.Connection.QueryAsync<Category>(
                query);
            return categories;
        }

        public Task<Category> GetById(int id)
        {
            var query = CategoryQueries.GetCategoryByIdQuery;
            var category = this.Connection.QueryFirstOrDefaultAsync<Category>(
                query, new { id });
            return category;
        }

        public async Task Update(Category entity)
        {
            var query = CategoryQueries.UpdateCategoryQuery;
            await this.Connection.ExecuteAsync(query, new { entity.Name, entity.Description, entity.IsAvailable, entity.Id });
        }

        public async Task<IEnumerable<Category>> GetAllBySiteName(string applicationName)
        {
            var query = CategoryQueries.GetAllCategoriesBySiteNameQuery;
            var categories = await this.Connection.QueryAsync<Category>(query, new { applicationName });
            return categories;
        }
    }
}
